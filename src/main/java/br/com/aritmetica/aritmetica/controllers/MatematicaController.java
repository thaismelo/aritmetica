package br.com.aritmetica.aritmetica.controllers;

import br.com.aritmetica.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Informe pelo menos dois numeros para serem somados!");
        }

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO){
        List<Integer> numeros = entradaDTO.getNumeros();

        if (numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Informe pelo menos dois numeros para serem subtraídos!");
        }

        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO){
        List<Integer> numeros = entradaDTO.getNumeros();

        if (numeros.isEmpty() || numeros.size() != 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Informe apenas dois numeros para serem multiplicados");
        }

        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }

    @PutMapping("divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO){
        List<Integer> numeros = entradaDTO.getNumeros();

        if (numeros.isEmpty() || numeros.size() != 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Informe apenas doi numeros para serem dividos!");
        }
        else if(numeros.get(1) > numeros.get(0))
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Não é possível dividir um número por outro menor!");
        }

        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;
    }

}
