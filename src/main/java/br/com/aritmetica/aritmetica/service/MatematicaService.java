package br.com.aritmetica.aritmetica.service;

import br.com.aritmetica.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO) {
        int numero = 0;

        for(int n: entradaDTO.getNumeros()) {
            numero += n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = 0;
        for(int n: entradaDTO.getNumeros()){
            if (entradaDTO.getNumeros().indexOf(n) == 0){
                numero = n;
            } else {
                numero -= n;
            }
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int numero = 1;
        for(int n: entradaDTO.getNumeros()){
            numero *= n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int numero = 0;
        numero = entradaDTO.getNumeros().get(0) / entradaDTO.getNumeros().get(1);

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

}
