package br.com.aritmetica.aritmetica.DTOs;

import java.util.ArrayList;
import java.util.List;

public class EntradaDTO {
    private List<Integer> numeros;

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
